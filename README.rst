closeall - firefox bug 147659 workaround
========================================
Prevents file descriptors leak by running program with
all descriptors closed except for 0,1 and 2.

Usage
-----
See ff-xdg-open-wrapper for example.
