#include <stdlib.h>
#include <stdio.h>
#include <sys/resource.h>
#include <unistd.h>

int main(int argc, char** argv) {
    struct rlimit lim;

    if (argc > 1) {
        getrlimit(RLIMIT_NOFILE, &lim);
        for (int i = 3; i < lim.rlim_max; i++)
            close(i);
        execvp(argv[1], argv+1);
        perror(argv[1]);
    }
    fputs("usage: closeall COMMAND [ARG]...\n", stderr);
    return 1;
}
